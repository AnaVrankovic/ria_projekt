<?php
use Phalcon\Mvc\Model;

class pas extends Model {
	public $idpas;
	public $ime_psa;
	public $pasmina;
	public $kategorija;
	public $vlasnik;
	public function initialize() {
		$this -> setConnectionService('mkk');
		 $this->belongsTo("idpas", "prijave", "pas");
		 $this->hasOne("vlasnik", "korisnik", "email");
	}
	
	public function setIme($name){
		$this->ime_psa=$name;
		
	}
	public function setVlasnik($email){
		$this->vlasnik=$email;
		
	}
	public function setPasmina($pass){
		$this->pasmina=$pass;
		
	}
	public function setKat($kat){
		$this->kategorija=$kat;
		
	}
	public function setId($id){
		$this->idpas=$id;
		
	}	
}
?>