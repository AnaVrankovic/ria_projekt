<?php
use Phalcon\Mvc\Model;

class novost extends Model {
	public $naslov;
	public $idNaslov;
	public $sadrzaj;
	
	public function initialize() {
		$this -> setConnectionService('mkk');
		
	}
	
	public function setNaslov($title){
		$this->naslov=$title;
		
	}
	public function setSadrzaj($content){
		$this->sadrzaj=$content;
		
	}
	
}
?>