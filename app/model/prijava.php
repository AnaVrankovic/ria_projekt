<?php
use Phalcon\Mvc\Model;

class prijava extends Model {
	public $idprijava;
	public $iddogadaj;
	public $idpas;
	
	public function initialize() {
		$this -> setConnectionService('mkk');		
        $this->hasMany("iddogadaj", "dogadaj", "iddogadaj");
    	$this->hasMany("pas", "pas", "idpas");
	}
	public function setIdDogadaj($name){
		$this->iddogadaj=$name;
		
	}
	public function setPas($dog){
		$this->idpas=$dog;
		
	}
	
}
?>