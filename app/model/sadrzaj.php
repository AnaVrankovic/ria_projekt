<?php
use Phalcon\Mvc\Model;

class sadrzaj extends Model {
	public $idsadrzaj;
	public $imesadrzaj;
	public $tekstsadrzaj;
	public function initialize() {
		$this -> setConnectionService('mkk');
	}
	public function setId($id){
		$this->idsadrzaj=$id;
		
	}
	public function setIme($name){
		$this->imesadrzaj=$name;
		
	}
	public function setTekst($tekst){
		$this->tekstsadrzaj=$tekst;
		
	}
	
}
?>