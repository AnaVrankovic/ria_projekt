<?php
use Phalcon\Mvc\Model;

class dogadaj extends Model {
	public $iddogadaj;
	public $ime_dogadaj;
	public $datum;
	public $vrijeme;
	public $opis;
	public $autor;
	public $mjesto;
	public function initialize() {
		$this -> setConnectionService('mkk');
		 $this->belongsTo("iddogadaj", "prijave", "iddogadaj");
	}
	
	public function setIme($name){
		$this->ime_dogadaj=$name;
		
	}
	public function setDatum($datum){
		$this->datum=$datum;
		
	}
	public function setVrijeme($vri){
		$this->vrijeme=$vri;
		
	}
	public function setOpis($opis){
		$this->opis=$opis;
		
	}
	public function setAutor($autor){
		$this->autor=$autor;
		
	}
	public function setMjesto($mjesto){
		$this->mjesto=$mjesto;
		
	}
	
}
?>