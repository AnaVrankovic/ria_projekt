<?php
use Phalcon\Mvc\Model;

class korisnik extends Model {
	public $ime;
	public $email;
	public $pass;
	public $privilegije;
	public function initialize() {
		$this -> setConnectionService('mkk');
		$this->belongsTo("email", "pas", "vlasnik");
	}
	public function setIme($name){
		$this->ime=$name;
		
	}
	public function setEmail($email){
		$this->email=$email;
		
	}
	public function setPas($pass){
		$this->pass=$pass;
		
	}
	public function setPriv($privilegije){
		$this->privilegije=$privilegije;
		
	}
}
?>