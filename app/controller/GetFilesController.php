<?php
use Phalcon\Mvc\Controller;

class GetFilesController extends Controller {
	public function indexAction() {
		$this -> view -> disable();
		$response = new \Phalcon\Http\Response();
		
        $list=glob("./Projekt/public/uploads/files/*");
        foreach($list as $value){
        	    $files = explode("/", $value);
                $json[] = array('file'=>$files[5]);
}
		
        $response -> setStatusCode(200, "OK");
        $response -> setContent(json_encode($json));
        return $response;
	}

	public function notFoundAction() {
		// Send a HTTP 404 response header
		$response -> setStatusCode(404, "Not Found");
		return $response;
	}}