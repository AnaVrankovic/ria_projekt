<?php
use Phalcon\Mvc\Controller;

class GetPicturesController extends Controller {
	public function indexAction() {
		$this -> view -> disable();
		$response = new \Phalcon\Http\Response();
		
        $list=glob("./Projekt/public/uploads/pictures/*");
        foreach($list as $value){
        	    $files = explode("/", $value);
                $json[] = array('src'=>$files[5]);
}
		
        $response -> setStatusCode(200, "OK");
        $response -> setContent(json_encode($json));
        return $response;
	}

	public function notFoundAction() {
		// Send a HTTP 404 response header
		$response -> setStatusCode(404, "Not Found");
		return $response;
	}}