<?php
    
use Phalcon\Mvc\Controller;

class UploadPicController extends Controller
{

    public function indexAction()
    {
		$this->view->disable();
		$response = new \Phalcon\Http\Response();
		
		$putdata = fopen("php://input", "r");
		if ( !empty( $_FILES ) ) {
    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
    $uploadPath = "uploads/pictures/". $_FILES[ 'file' ][ 'name' ];
    move_uploaded_file( $tempPath, $uploadPath );
    $answer = array( 'answer' => $_FILES[ 'file' ][ 'name' ] );
    $json = json_encode( $answer );
    echo $json;
} else {
    echo 'No files';
}
	}

 public function notFoundAction()
    {
        // Send a HTTP 404 response header
        $response->setStatusCode(404, "Not Found");
		return $response;
    }
}
    
    
    
?>
