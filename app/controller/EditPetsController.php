<?php
    
use Phalcon\Mvc\Controller;

class EditPetsController extends Controller
{

    public function indexAction()
    {
    	$this->view->disable();     
        $response = new \Phalcon\Http\Response();

       
    	if ($this->request->isPost() == true) {
        	$sadrzaj = new pas();
			$data = file_get_contents("php://input");
        	$data = json_decode($data, TRUE);
			$sadrzaj->setVlasnik($data["vlasnik"]);
			$sadrzaj->setIme($data["ime"]);
			$sadrzaj->setKat($data["kat"]);
			$sadrzaj->setPasmina($data["pasmina"]);
	 		$sadrzaj->setId($data["id"]);
		 $success =$sadrzaj->update();
			
			
        // Store and check for errors
       //  $success = $dogadaj->save();

        if ($success) {
            $response->setStatusCode(200);
			$response->setContent("Izmjenjen ljubimac");
			return $response;
        } else {
            $mess="Sorry, the following problems were generated: ";
            foreach ($sadrzaj->getMessages() as $message) {
                $mess=$mess.$message->getMessage()."\n";
            }$response->setStatusCode(404);
			$response->setContent($mess);
			return $response;
        }

		}
    }
	 
	 public function notFoundAction()
    {
        // Send a HTTP 404 response header
        $response->setStatusCode(404, "Not Found");
		return $response;
    }
}
    
    
    
?>