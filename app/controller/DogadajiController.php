<?php

use Phalcon\Mvc\Controller;

class DogadajiController extends Controller {

	public function indexAction() {
		$this -> view -> disable();
		$response = new \Phalcon\Http\Response();
		$today=date('y-m-d');
		$dogadaji = dogadaj::query()  
					  ->where(  "datum>=:today:")  
					   ->bind(array("today" =>$today))          
                      ->order("datum")
                 ->execute();
	
		foreach ( $dogadaji as $data ) {
			    $result = korisnik::query()
    ->where("email = :email:")
    ->bind(array("email" =>$data->autor))
    ->execute();
	foreach ( $result as $ime) {
		 $test = new DateTime($data->vrijeme);
        $date= date_format($test, 'H:i');
		   
                $json[] = array(
                'imeAutor'=>$ime->ime,
                'autor'=>$data->autor,
                'dogadaj'=>$data->ime_dogadaj,
                'datum'=>$data->datum,
                'vrijeme'=>$date,
				'opis'=>$data->opis,
				'mjesto'=>$data->mjesto,
				'id'=>$data->iddogadaj
				);
        }}
	
		
		$response -> setStatusCode(200, "OK");
		
		$response -> setContent( json_encode($json));
		return $response;
	}

	public function notFoundAction() {
		// Send a HTTP 404 response header
		$response -> setStatusCode(404, "Not Found");
		return $response;
	}

}
?>