<?php

use Phalcon\Mvc\Controller;

class PrijaveController extends Controller {

	public function indexAction() {
		$this -> view -> disable();
		$response = new \Phalcon\Http\Response();
		
		$dogadaji = dogadaj::find();
		$mess=" dogadaji: ";
		foreach ( $dogadaji as $idDogadaj ) {
				$prijava = prijava::query()
    ->where("iddogadaj = :id:")
    ->bind(array("id" =>$idDogadaj->iddogadaj))
    ->execute();
			   
				foreach ( $prijava as $idPrijava ) {
					$pas = pas::query()
    					->where("idpas = :id:")
   					    ->bind(array("id" =>$idPrijava->idpas))
    					->execute();
					
					
					foreach ( $pas as $idPas ) {
						$vlasnik = korisnik::query()
    					->where("email = :id:")
   					    ->bind(array("id" =>$idPas->vlasnik))
    					->execute();
						foreach ( $vlasnik as $data) {
							$json[] = array(
							'idPrijave'=>$idPrijava->idprijave,
							'imeDogadaja'=>$idDogadaj->iddogadaj,
							'pas'=>$idPas->ime_psa,
							'pasmina'=>$idPas->pasmina,
							'kategorija'=>$idPas->kategorija,
							'ime'=>$data->ime,
                			'email'=>$data->email)
							;
					}
						
							
                			
					}
					
					
				}
				
				
	}
	
		
		$response -> setStatusCode(200, "OK");
		
		$response -> setContent( json_encode($json));
		return $response;
	}

	public function notFoundAction() {
		// Send a HTTP 404 response header
		$response -> setStatusCode(404, "Not Found");
		return $response;
	}

}
?>