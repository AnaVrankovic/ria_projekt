<?php
    
use Phalcon\Mvc\Controller;

class EditController extends Controller
{

    public function indexAction()
    {
    	$this->view->disable();     
        $response = new \Phalcon\Http\Response();

       
    	if ($this->request->isPost() == true) {
        	$sadrzaj = new sadrzaj();
			$data = file_get_contents("php://input");
        	$data = json_decode($data, TRUE);
			$sadrzaj->setIme($data["naslov"]);
			$sadrzaj->setTekst($data["sadrzaj"]);
	 		$sadrzaj->setId($data["id"]);
		 $update =$sadrzaj->update();
			
			if(!$update){
				
				
				$success =$sadrzaj->save();
				
			}
			
        // Store and check for errors
       //  $success = $dogadaj->save();

        if ($success) {
            $response->setStatusCode(200);
			$response->setContent("Unesen novi sadrzaj");
			return $response;
        } else {
            $mess="Sorry, the following problems were generated: ";
            foreach ($sadrzaj->getMessages() as $message) {
                $mess=$mess.$message->getMessage()."\n";
            }$response->setStatusCode(404);
			$response->setContent($mess);
			return $response;
        }

		}
    }
	 
	 public function notFoundAction()
    {
        // Send a HTTP 404 response header
        $response->setStatusCode(404, "Not Found");
		return $response;
    }
}
    
    
    
?>