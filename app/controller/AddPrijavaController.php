<?php
    
use Phalcon\Mvc\Controller;

class AddPrijavaController extends Controller
{

    public function indexAction()
    {
    	$this->view->disable();     
        $response = new \Phalcon\Http\Response();

       
    	if ($this->request->isPost() == true) {
        	$prijava = new prijava();
			$data = file_get_contents("php://input");
        	$data = json_decode($data, TRUE);		
			$prijava->setIdDogadaj($data["idevent"]);
			$prijava->setPas($data["name"]);
			
			
			
        // Store and check for errors
        $success = $prijava->save();

        if ($success) {
            $response->setStatusCode(200);
			$response->setContent("Prijava uspijesna");
			return $response;
        } else {
            $mess="Sorry, the following problems were generated: ";$mess=$mess.$data["name"];
            foreach ($prijava->getMessages() as $message) {
                $mess=$mess.$message->getMessage()."\n";
            }$response->setStatusCode(404);
			$response->setContent($mess);
			return $response;
        }

		}
    }
	 
	 public function notFoundAction()
    {
        // Send a HTTP 404 response header
        $response->setStatusCode(404, "Not Found");
		return $response;
    }
}
    
    
    
?>