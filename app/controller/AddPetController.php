<?php
    
use Phalcon\Mvc\Controller;

class AddPetController extends Controller
{

    public function indexAction()
    {
    	$this->view->disable();     
        $response = new \Phalcon\Http\Response();

       
    	if ($this->request->isPost() == true) {
        	$pas= new pas();
			$data = file_get_contents("php://input");
        	$data = json_decode($data, TRUE);		
			$pas->setIme($data["ime"]);
			$pas->setVlasnik($data["email"]);
			$pas->setKat($data["kat"]);
			$pas->setPasmina($data["pasmina"]);
			
        // Store and check for errors
        $success = $pas->save();

        if ($success) {
            $response->setStatusCode(200);
			$response->setContent("Prijava uspijesna");
			return $response;
        } else {
            $mess="Sorry, the following problems were generated: ";
            foreach ($pas->getMessages() as $message) {
                $mess=$mess.$message->getMessage()."\n";
            }$response->setStatusCode(404);
			$response->setContent($mess);
			return $response;
        }

		}
    }
	 
	 public function notFoundAction()
    {
        // Send a HTTP 404 response header
        $response->setStatusCode(404, "Not Found");
		return $response;
    }
}
    
    
    
?>