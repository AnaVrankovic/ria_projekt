<?php
use Phalcon\Mvc\Controller;

class RegisterController extends Controller
{

    public function indexAction()
    {
    	$this->view->disable();        
        $response = new \Phalcon\Http\Response();      	 
		if ($this->request->isPost() == true) {
        	$user = new korisnik();
			$data = file_get_contents("php://input");
        	$data = json_decode($data, TRUE);
			$user->setIme($data["name"]);
			$user->setEmail($data["email"]);
			$pass=$data["pass"];//odavde mjenjam
			$hash= password_hash($pass, PASSWORD_BCRYPT);
			$user->setPas($hash);//do tu mjenjam
			$user->setPriv('r');
			
        // Store and check for errors
        $success = $user->save();

        if ($success) {
            $response->setStatusCode(200);
			$response->setContent("Registered");
			return $response;
        } else {
            $mess="Sorry, the following problems were generated: ";
            foreach ($user->getMessages() as $message) {
                $mess=$mess.$message->getMessage()."\n";
            }$response->setStatusCode(404);
			$response->setContent($mess);
			return $response;
        }

		}
    }
	 public function registerAction()
    {	
        
		
    }
	 public function notFoundAction()
    {
        // Send a HTTP 404 response header
        $this->response->setStatusCode(404, "Not Found");
    }
}
?>