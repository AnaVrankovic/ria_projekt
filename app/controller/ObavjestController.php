<?php

use Phalcon\Mvc\Controller;

class ObavjestController extends Controller {

	public function indexAction() {
		$this -> view -> disable();
		$response = new \Phalcon\Http\Response();
		$obavjesti = novost::find();
	
		foreach ( $obavjesti as $data ) {
  
	
		$json[] = array(
						'naslov'=>$data->naslov,
                		'sadrzaj'=>$data->sadrzaj,
                		'idNaslov'=>$data->idNaslov
                				);
        }
		$json_reverse = array_reverse($json);
	
		
		$response -> setStatusCode(200, "OK");
		
		$response -> setContent( json_encode($json_reverse));
		return $response;
		
	
	}

	public function notFoundAction() {
		// Send a HTTP 404 response header
		$response -> setStatusCode(404, "Not Found");
		return $response;
	}

}
?>