<?php
    
use Phalcon\Mvc\Controller;

class AddDogadajController extends Controller
{

    public function indexAction()
    {
    	$this->view->disable();     
        $response = new \Phalcon\Http\Response();

       
    	if ($this->request->isPost() == true) {
        	$dogadaj = new dogadaj();
			$data = file_get_contents("php://input");
        	$data = json_decode($data, TRUE);
			
			$dat=date('y.m.d',strtotime($data["date"]));
			$dogadaj->setIme($data["name"]);
			$dogadaj->setDatum($dat);
			$dogadaj->setVrijeme($data["time"]);
			$dogadaj->setMjesto($data["place"]);
			$dogadaj->setOpis($data["des"]);
			$result = korisnik::query()
    ->where("ime = :ime:")
    ->bind(array("ime" => $data["user"]))
    ->execute();
	  foreach ( $result as $kor ) {
               $dogadaj->setAutor($kor->email);
        }
			
        // Store and check for errors
        $success = $dogadaj->save();

        if ($success) {
            $response->setStatusCode(200);
			$response->setContent("Unesen novi dogadaj".$dat." ".$data["date"]);
			return $response;
        } else {
            $mess="Sorry, the following problems were generated: ";$mess=$mess.$data["name"];
            foreach ($dogadaj->getMessages() as $message) {
                $mess=$mess.$message->getMessage()."\n";
            }$response->setStatusCode(404);
			$response->setContent($mess);
			return $response;
        }

		}
    }
	 
	 public function notFoundAction()
    {
        // Send a HTTP 404 response header
        $response->setStatusCode(404, "Not Found");
		return $response;
    }
}
    
    
    
?>