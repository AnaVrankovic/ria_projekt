	var app = angular.module('myApp', ['500tech.simple-calendar']);
	app.directive('googleMap', function() {
		return {
			template : '<iframe width="50%" height="150" frameborder="0" style="border:0"></iframe>',
			restrict : 'E',
			scope : {
				address : '='
			},
			link : function postLink(scope, element) {
				var mapFrame = element.find("iframe");
				if (scope.address) {
					mapFrame.attr('src', "https://www.google.com/maps/embed/v1/place?q=" + scope.address + "&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU");
				} else {
					mapFrame.attr('src', '');
				}
			}
		};
	});

   
   
    app.directive('fileModel', ['$parse', function ($parse) {
            return {
               restrict: 'A',
               link: function(scope, element, attrs) {
                  var model = $parse(attrs.fileModel);
                  var modelSetter = model.assign;
                  
                  element.bind('change', function(){
                     scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                     });
                  });
               }
            };
         }]);
      
   

   
   
   
	app.controller('myCtrl', function($scope, $http, $window) {
		$scope.loged = false;
		$scope.editinfshow = false;
		$scope.editpetshow=false;
		$scope.uploaded=false;
		$scope.idToDelete;
		
		$scope.setUser = function() {

			$scope.session = $window.sessionStorage.getItem("user");
			$scope.priv = $window.sessionStorage.getItem("priv");
			
			if (($scope.session != "") && ($scope.session != null))
				$scope.loged = true;
			else{
				$scope.loged = false;
				$window.sessionStorage.setItem("userEmail","");}
		}
		$scope.setUser();
		$scope.lisucc = true;
		$scope.succ = false;
		$scope.addsucc = false;
		$scope.addfail = false;
		$scope.tab = 1;
		$scope.passNotMatch=false;
		$scope.setTab = function(newTab) {
			$scope.tab = newTab;
		};
		$scope.isSet = function(tabNum) {
			return $scope.tab === tabNum;
		};
		$scope.isAutor = function($autor) {
			return ($autor===$window.sessionStorage.getItem("userEmail"));
		};
		$scope.getPets = function() {
			$scope.userEmail=$window.sessionStorage.getItem("userEmail");
			 $scope.data = JSON.stringify({
					email :  $scope.userEmail
					
				});
			$http.post("./Pets",$scope.data).success(function(data, status) {
			$scope.pets = data;
		});};
		$scope.register = function() {

			if ($scope.registration.$invalid ) {
				return;
			}
			if( $scope.pass!=$scope.passrepeat )
			{
				$scope.passNotMatch=true;
				$scope.response = "Unesene lozinke nisu jednake!";
				$scope.pass="";
				$scope.passrepeat="";
				return;
			}
			
			$scope.data = JSON.stringify({
				name : $scope.name,
				pass : $scope.pass,
				email : $scope.email
			});

			$scope.config = {
				headers : {
					'Content-Type' : 'application/json;charset=utf-8;'
				}
			}
			$http.post("./Register", $scope.data, $scope.config).success(function(data, status, content) {
				$scope.response = "Hvala na registraciji!";
				$scope.name = "";
				$scope.email = "";
				$scope.pass = "";
				$scope.succ = true;
			});

		}
		$scope.login = function() {

			if ($scope.login.$invalid) {
				return;
			}
			$scope.lidata = JSON.stringify({
				pass : $scope.lipass,
				email : $scope.liemail
			});

			$scope.liconfig = {
				headers : {
					'Content-Type' : 'application/json;charset=utf-8;'
				}
			}
			$http.post("./Login", $scope.lidata, $scope.liconfig).success(function(data, status) {

				$scope.liresponse = data;

				$scope.liemail = "";
				$scope.lipass = "";
				if ($scope.liresponse != 'null') {
					$scope.lisucc = true;
					$scope.userEmail=$scope.liresponse[0].email;
					$window.sessionStorage.setItem("user", $scope.liresponse[0].name);
					$window.sessionStorage.setItem("priv", $scope.liresponse[0].privilegije);
					$window.sessionStorage.setItem("userEmail", $scope.liresponse[0].email);
					$scope.tab = 1;
					$scope.setUser();
				} else
					$scope.lisucc = false;
			});

		}
		
		$http.get("./Dogadaji").success(function(data, status) {
			$scope.dogadaji = data;
			$scope.i=0;
			
		});
		//odavde
		$http.get("./Obavjest").success(function(data, status) {
			$scope.obavjesti = data;
			
			
		});
		//do tu
		$http.get("./Calendar").success(function(data, status) {
			$scope.eventsx = data;
			
			
		});
		$http.get("./Prijave").success(function(data, status) {
			$scope.prijave = data;
		});
	
		$http.get("./Informacije").success(function(data, status) {
			$scope.informacije = data;
		});
		$scope.admin = function() {
			if ($scope.session == null)
				return false;
			else {
				if ($scope.priv == "a")
					return true;
				else
					return false;

			}

		}
		$scope.deletedogadaj = function() {
			$scope.del = JSON.stringify({
				id : $scope.idToDelete,
				user : $scope.session
			});

			$scope.delconfig = {
				headers : {
					'Content-Type' : 'application/json;charset=utf-8;'
				}
			}

			$http.post("./DeleteDogadaj", $scope.del, $scope.delconfig).success(function(data, status) {
				$window.location.reload();
			});

		}
		
		$scope.adddogadaj = function() {
			$scope.dateevent=$scope.datum.toLocaleDateString();
			$scope.timeevent=$scope.vrijeme.toLocaleTimeString();
			$scope.add = JSON.stringify({
				name : $scope.imedogadaj,
				time : $scope.timeevent,
				date : $scope.dateevent,
				des : $scope.opis,
				place : $scope.mjesto,
				user : $scope.session
			});

			$scope.addconfig = {
				headers : {
					'Content-Type' : 'application/json;charset=utf-8;'
				}
			}

			$http.post("./AddDogadaj", $scope.add, $scope.addconfig).success(function(data, status) {

				$scope.addresponse = "Uspješno dodan događaj!";

				$scope.addresponse = "Uspješno dodan događaj!";
				$scope.addsucc = true;
				$scope.dodajdogadaj = !$scope.dodajdogadaj;

			}).error(function(data, status, headers, config) {
				$scope.addfail = true;
				$scope.addresponse = "Neuspješno dodan događaj!";
			});

		}
		$scope.formdogadaj = function(id) {

			$scope.dodajdogadaj = true;

		}
				$scope.addobavjest = function() {
			$scope.add = JSON.stringify({
				title : $scope.naslovobavjest,
				content : $scope.sadrzaj,

			});

			$scope.addconfig = {
				headers : {
					'Content-Type' : 'application/json;charset=utf-8;'
				}
			}

			$http.post("./AddObavjest", $scope.add, $scope.addconfig).success(function(data, status) {

				$scope.addresponse = "Uspješno dodansa obavjest!";

				$scope.addresponse = "Uspješno dodana obavjest!";

				$scope.addsucc = true;
				$scope.dodajdogadaj = !$scope.dodajdogadaj;

			}).error(function(data, status, headers, config) {
				$scope.addfail = true;
				$scope.addresponse = "Neuspješno dodana obavjest!";
			});

		}
		$scope.formobavjest = function(id) {

			$scope.dodajobavjest = true;
			$window.location.reload();

		}
		
		$scope.logout = function() {

			$window.sessionStorage.setItem("user", "");
			$window.sessionStorage.setItem("priv", "");
			$window.sessionStorage.setItem("userEmail", "");
			$scope.setUser();
		}
		$scope.emailsErolled=function($EventIdForEnrolled){
			
			$scope.eventIdForEmails=$EventIdForEnrolled;
			
			
		}
		$scope.editinf = function($imesadrzaj,$tekstsadrzaj) {
				$scope.add = JSON.stringify({
				naslov : $imesadrzaj,
				sadrzaj : $tekstsadrzaj,
				id : "informacije",
				
			});

			$scope.addconfig = {
				headers : {
					'Content-Type' : 'application/json;charset=utf-8;'
				}
			}

			$http.post("./Edit", $scope.add, $scope.addconfig).success(function(data, status) {
				
				$scope.editinfshow = false;
					$window.location.reload();

			});
		}
		
		$scope.editpets = function($idpsa,$imepsa,$pasmina,$kategorija) {
			$scope.add = JSON.stringify({
			id : $idpsa,
			ime: $imepsa,
			pasmina:$pasmina,
			kat:$kategorija,
			vlasnik:$window.sessionStorage.getItem("userEmail"),
		});

		$scope.addconfig = {
			headers : {
				'Content-Type' : 'application/json;charset=utf-8;'
			}
		}

		$http.post("./EditPets", $scope.add, $scope.addconfig).success(function(data, status) {
			
			//	$window.location.reload();

		});
	}
		$scope.addpet = function() {
			$scope.add = JSON.stringify({			
			ime: $scope.imePsa,
			pasmina:$scope.pasmina,
			kat:$scope.kategorija,
			email:$window.sessionStorage.getItem("userEmail"),
		});

		$scope.addconfig = {
			headers : {
				'Content-Type' : 'application/json;charset=utf-8;'
			}
		}

		$http.post("./AddPet", $scope.add, $scope.addconfig).success(function(data, status) {
			
				$window.location.reload();

		});
	}
		$scope.openpicture=function($url){
			$scope.site="./public/uploads/pictures/"+$url;
			var newWindowRef = $window.open($scope.site,$scope.site);
		}
		$scope.enroll = function(nameEvent,idEvent) {
				 
				 $window.sessionStorage.setItem("event", nameEvent);
				 $window.sessionStorage.setItem("idEvent", idEvent);
				
		
   			   $scope.left = screen.width/2 -250 ;
     		   $scope.top = screen.height/2-250;
			var newWindowRef = $window.open("./enroll", "Prijava", 'width=500,height=500,top=' + $scope.top + ', left=' + $scope.left+',resizable=1');
		
		}
		$scope.deleteModal=function($id){
			$scope.idToDelete=$id;
			
		}
		$scope.brisanjePrijave = function() {
			$scope.add = JSON.stringify({				
				id : $scope.idToDelete
			});
				$http.post("./DeletePrijava", $scope.add, $scope.addconfig).success(function(data, status) {
				$window.location.reload();
			});
		}
		$scope.deleteUser = function() {
			$scope.add = JSON.stringify({				
				id : $scope.idToDelete
			});
				$http.post("./Deleteuser", $scope.add, $scope.addconfig).success(function(data, status) {
				$scope.getAllUsers();
			});
		}
		$scope.deleteDog = function() {
			$scope.add = JSON.stringify({				
				id : $scope.idToDelete
			});
				$http.post("./DeleteDog", $scope.add, $scope.addconfig).success(function(data, status) {
				$window.location.reload();
			});
		}
		$scope.deleteObavjest = function() {
			$scope.add = JSON.stringify({				
				idNaslov : $scope.idToDelete
			});
				$http.post("./DeleteObavjest", $scope.add, $scope.addconfig).success(function(data, status) {
				$window.location.reload();
			});
		}
			
		$scope.deleteFile = function() {
			$scope.add = JSON.stringify({				
				id : $scope.idToDelete
			});
				$http.post("./DeleteFile", $scope.add, $scope.addconfig).success(function(data, status) {
				
			});
		}
				$http.get("./GetFiles").success(function(data, status) {
				$scope. filesForDownload=data;
			});
				
				$scope.uploadedFile = function(element) {
					 $scope.$apply(function($scope) {
					   $scope.files = element.files;         
					 });
					}
				
				$scope.addFile = function() {
					 $scope.uploadPic($scope.files,
					   function( msg ) // success
					   {
					    console.log('uploaded');
					   },
					   function( msg ) // error
					   {
					    console.log('error');
					   });
					}
				
		 $scope.uploadPic = function(){
               
			 
               var url = "./uploadPic";
               for ( var i = 0; i <  $scope.files.length; i++)
               {
                var fd = new FormData();
               
                fd.append("file",  $scope.files[i]);
               
                $http.post(url, fd, {
                
                 withCredentials : false,
                
                 headers : {
                  'Content-Type' : undefined
                 },
               transformRequest : angular.identity

               })
               .success(function(data)
               {
                console.log(data);
               })
               .error(function(data)
               {
                console.log(data);
               });
              }
              }
               
            ;
		$scope.deleteFile = function() {
			$scope.add = JSON.stringify({				
				id : $scope.idToDelete
			});
				$http.post("./DeleteFile", $scope.add, $scope.addconfig).success(function(data, status) {
				
			});
		}
				$http.get("./GetFiles").success(function(data, status) {
				$scope. filesForDownload=data;
			});
		
		 $scope.uploadFile = function(){
               var file = $scope.myFile;
               $scope.uploaded=true;
               console.log('file is ' );
               console.dir(	$scope.uploaded);
               
               var uploadUrl = "./Upload";
              
               var fd = new FormData();
               fd.append('file', file);
            
               $http.post(uploadUrl, fd, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
               })
            
               .success(function(){
               
               	$scope.uploadMess="Upload uspiješan";
               })
            
               .error(function(){
              
               	$scope.uploadMess="Upload neuspiješan";
               });
            
            };
            $scope.uploadProfile = function(){
                var file = $scope.profPic;
               file.filename=$scope.session;
                
                var uploadUrl = "./UploadProfPic";
               
                var fd = new FormData();
                fd.append('file', file);
                
    		
                $http.post(uploadUrl,fd,{
                   
                   headers: {'Content-Type': undefined}
                })
             
                .success(function(){
                
                	$scope.uploadMess="Upload uspiješan";
                	//$window.location.reload();
                	$scope.tab ==='8';
                })
             
                .error(function(){
               
                	$scope.uploadMess="Upload neuspiješan";
                });
             
             };
            $today = new Date();
			$yesterday = new Date($today);
			$yesterday.setDate($today.getDate() - 1);
            $scope.calendarOptions = {
    defaultDate: $today,
    minDate: $yesterday,
    maxDate: new Date([2030, 12, 31]),
    dayNamesLength: 1, // How to display weekdays (1 for "M", 2 for "Mo", 3 for "Mon"; 9 will show full day names; default is 1)
    eventClick: $scope.eventClick,
    dateClick: $scope.dateClick
  };
            $http.get("./GetPictures").success(function(data, status) {		                   
            $scope.pictures= data;

            });    
           $scope.getAllUsers=function(){
        	   $http.get("./Getallusers").success(function(data, status) {		                   
           
                $scope.allUsers= data;

                });};  

       
});
 


